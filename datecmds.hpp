//    Copyright (C) 2017, 2019 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DATE_CMDS_HPP
#define DATE_CMDS_HPP

#include <functional>
#include <map>
#include <memory>
#include <boost/date_time/gregorian/gregorian.hpp>


struct bill_data {
    enum class period_type { day_of_week, day_of_month, day_of_year };

    std::string nam;
    int spn;
    char typ;

    period_type ptyp;
    int day;
    int month;
    int weekday; // 0-6, sun...
};


void print_heading(
    boost::gregorian::date date,
    int span,
    int indent
    );

void process_bills_file(
    const char* bills_file,
    std::function<void (const bill_data& bdata)> process
    );

std::map<std::string,boost::gregorian::date>
parse_bills_paid_file(
    const std::string& paid_file
    );

#endif // DATE_CMDS_HPP
