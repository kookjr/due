//    Copyright (C) 2017, 2019, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PERIOD_HPP
#define PERIOD_HPP

#include <cctype>
#include <boost/date_time/gregorian/gregorian.hpp>

class DayOfTheWeek {
public:
    DayOfTheWeek(int rlen, int dow, char t='x') :
        range_len(rlen),
        day_of_week(dow),
        type(t) {
    }
    boost::gregorian::date nextPoint(const boost::gregorian::date& dt) const {
        int dow_dt = dt.day_of_week();
        int add_days;
        if (dow_dt > day_of_week)
            add_days = (day_of_week + 7) - dow_dt;
        else
            add_days = day_of_week - dow_dt;
        return dt + boost::gregorian::days(add_days);
    }
    boost::gregorian::date earlyPoint(const boost::gregorian::date& dt) const {
        return nextPoint(dt) - boost::gregorian::days(range_len - 1);
    }
    char getType() const {
        return type;
    }
private:
    const int range_len;
    const int day_of_week;
    const char type;
};

class DayOfTheMonth {
public:
    DayOfTheMonth(int rlen, int d, char t='x') :
        range_len(rlen),
        day(d),
        type(t) {
    }
    boost::gregorian::date nextPoint(const boost::gregorian::date& dt) const {
        int lastday = boost::gregorian::gregorian_calendar::end_of_month_day(dt.year(), dt.month());
        if (day < lastday)
            lastday = day;
        boost::gregorian::date d(dt.year(), dt.month(), lastday);
        if (dt <= d)
            return d;
        return d + boost::gregorian::months(1);
    }
    boost::gregorian::date earlyPoint(const boost::gregorian::date& dt) const {
        return nextPoint(dt) - boost::gregorian::days(range_len - 1);
    }
    char getType() const {
        return type;
    }
private:
    const int range_len;
    const int day;
    const char type;
};

class DayOfTheYear {
public:
    DayOfTheYear(int rlen, int m, int d, char t='x') :
        range_len(rlen),
        day(d),
        month(m),
        type(t) {
    }
    boost::gregorian::date nextPoint(const boost::gregorian::date& dt) const {
        int lastday = boost::gregorian::gregorian_calendar::end_of_month_day(dt.year(), month);
        if (day < lastday)
            lastday = day;
        boost::gregorian::date d(dt.year(), month, lastday);
        if (dt <= d)
            return d;
        return d + boost::gregorian::years(1);
    }
    boost::gregorian::date earlyPoint(const boost::gregorian::date& dt) const {
        return nextPoint(dt) - boost::gregorian::days(range_len - 1);
    }
    char getType() const {
        return type;
    }
private:
    const int range_len;
    const int day;
    const int month;
    const char type;
};

template<class H>
char point_in_upcoming_range(
    boost::gregorian::date current,
    boost::gregorian::date last_paid,
    H& helper
    )
{
    auto point                = helper.nextPoint(current);
    auto earliest_point_range = helper.earlyPoint(current);

#if 0
    std::cerr
        << "-- cur=" << current
        << " -- p=" << point
        << " -- pE=" << earliest_point_range << std::endl;
#endif

    char r = '.';

    if (current >= earliest_point_range && current < point) {
        r = helper.getType();
        if (last_paid >= earliest_point_range)
            r = '*';
    }
    else if (current == point) {
        r = std::toupper(helper.getType());
        if (last_paid >= earliest_point_range)
            r = '*';
    }

    return r;
}

#endif // PERIOD_HPP
