//    Copyright (C) 2017, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <exception>
#include <memory>

#include <cstdlib>
#include <unistd.h>
#include <sys/ioctl.h>

#include <boost/program_options.hpp>

#include "clopts.hpp"
#include "datecmds.hpp"
#include "period.hpp"

#define NAME_WIDTH 10
#define DAY_WIDTH   3

int term_width() {
    int w = 80;

    struct winsize winsz;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &winsz) != -1)
        w = winsz.ws_col;

    return w;
}

int main(int argc, char* argv[]) {
    // default command line option values
    std::string billfile(std::string(getenv("HOME")) + "/.bills");
    std::string paidfile(std::string(getenv("HOME")) + "/.paid");

    // adjust number of days displayed based on the screen width
    int days = 22;
    int w = term_width();
    if (w  > (NAME_WIDTH + (5 * DAY_WIDTH))) {
        days = ((w - NAME_WIDTH) / DAY_WIDTH) - 1;
    }

    // update from command line options
    auto action = parse_command_line(argc, argv, billfile, paidfile, days);
    if (std::get<0>(action))
        return std::get<1>(action);

    // setup data and processing functions
    auto bills_paid    = parse_bills_paid_file(paidfile);
    auto today         = boost::gregorian::day_clock::local_day();
    auto process_bills = [today,days,&bills_paid](const bill_data& bdata) {
        boost::gregorian::date pdate(boost::gregorian::from_simple_string("2000-01-01"));
        auto const pp = bills_paid.find(bdata.nam);
        if (pp != bills_paid.end())
            pdate = pp->second;

        std::cout << std::setw(NAME_WIDTH) << bdata.nam.substr(0, NAME_WIDTH);
        for (int i=0; i < days; ++i) {
            boost::gregorian::days incr(i);
            if (bdata.ptyp == bill_data::period_type::day_of_week) {
                int day = bdata.day - 100;
                auto h = DayOfTheWeek(bdata.spn, day, bdata.typ);
                std::cout << "  " << point_in_upcoming_range(today + incr, pdate, h);
            }
            else if (bdata.ptyp == bill_data::period_type::day_of_month) {
                auto h = DayOfTheMonth(bdata.spn, bdata.day, bdata.typ);
                std::cout << "  " << point_in_upcoming_range(today + incr, pdate, h);
            }
            else if (bdata.ptyp == bill_data::period_type::day_of_year) {
                auto h = DayOfTheYear(bdata.spn, bdata.month, bdata.day, bdata.typ);
                std::cout << "  " << point_in_upcoming_range(today + incr, pdate, h);
            }
        }
        std::cout << std::endl;
    };

    // output heading and bill data
    print_heading(today, days, NAME_WIDTH);
    process_bills_file(billfile.c_str(), process_bills);

    return EXIT_SUCCESS;
}
