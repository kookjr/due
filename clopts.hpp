//    Copyright (C) 2017 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CLOPTS_HPP
#define CLOPTS_HPP

#include <string>
#include <tuple>

/*
 * Parse command line options, and return:
 *   <0> bool, if true exit program do not continue
 *   <1> int program exit value if <0> is true
 */
std::tuple<bool,int> parse_command_line(
    int argc, char* argv[],
    std::string& bill_file,
    std::string& paid_file,
    int& day_range);

#endif // CLOPTS_HPP
