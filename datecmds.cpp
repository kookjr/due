//    Copyright (C) 2017, 2019 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <algorithm>

#include "nlohmann/json.hpp"

#include "datecmds.hpp"

using json = nlohmann::json;

// the number corresponds to boost day_of_week values
static std::map<std::string,int> daw_name2int{
    { "sun", 0 },
    { "mon", 1 },
    { "tue", 2 },
    { "wed", 3 },
    { "thu", 4 },
    { "fri", 5 },
    { "sat", 6 },
};

void process_bills_file(
    const char* bills_file,
    std::function<void (const bill_data& bdata)> process
    ) {

    try {
        std::ifstream in(bills_file);
        json js = json::parse(in);

        // some quick validation of outer structure
        if (! js["bills"].is_array()) {
            std::cerr << "no bills in file" << std::endl;
            return;
        }

        for (const auto& bi : js["bills"]) {
            bill_data bd;

            // validation of each bill
            if (! (bi.contains("name") &&
                   bi.contains("days-prior") &&
                   bi.contains("bill-type")) )
                continue;

            bd.nam = bi.at("name");
            bd.spn = bi.at("days-prior");
            bd.typ = bi.at("bill-type").get<std::string>().at(0);

            if (bi.contains("dom")) {
                bd.ptyp = bill_data::period_type::day_of_month;
                bd.day = bi.at("dom");
            }
            else if (bi.contains("doy")) {
                bd.ptyp = bill_data::period_type::day_of_year;
                bd.month = bi.at("doy")[0];
                bd.day = bi.at("doy")[1];
            }
            else if (bi.contains("dow")) {
                bd.ptyp = bill_data::period_type::day_of_week;
                std::string daystr{bi.at("dow")};
                std::transform(daystr.begin(), daystr.end(), daystr.begin(), ::tolower);
                auto dp = daw_name2int.find(daystr);
                if (dp != daw_name2int.end()) {
                    bd.day = 100 + dp->second;
                }
                else {
                    std::cerr << "invalid day of month: " << daystr << std::endl;
                    continue;
                }
            }
            else {
                continue;
            }

            // call process with bill data
            process(bd);
        }
    }
    catch(json::basic_json::parse_error& e) {
        std::cerr << "Bill parsing exception: " << e.what()
                  << ", at " << e.byte
                  << std::endl;
    }
    catch(std::exception& e) {
        std::cerr << "Error reading bills file: " << e.what()
                  << std::endl;
    }
}

std::map<std::string,boost::gregorian::date>
parse_bills_paid_file(
    const std::string& paid_file
    )
{
    std::map<std::string,boost::gregorian::date> b;
    std::string name, pdate;
    try {
        std::ifstream in(paid_file);
        json js = json::parse(in);

        // some quick validation of outer structure
        if (! js["bills"].is_array()) {
            std::cerr << "no bills in file" << std::endl;
            return b;
        }
        for (const auto& bi : js["bills"]) {
            // validate paid data
            if (! (bi.contains("name") && bi.contains("paid")) ) {
                std::cerr << "invalid paid format" << std::endl;
                continue;
            }

            name  = bi.at("name");
            pdate = bi.at("paid");

            try {
                boost::gregorian::date d(boost::gregorian::from_simple_string(pdate));
                b[name] = d;
            }
            catch(std::exception& e) {
                std::cout << "warning: invalid paid date ["
                          <<  pdate << "]"
                          << std::endl;
            }
        }
    }
    catch(json::basic_json::parse_error& e) {
        std::cerr << "Bill parsing exception: " << e.what()
                  << ", at " << e.byte
                  << std::endl;
    }
    catch(std::exception& e) {
        std::cerr << "Error reading bills file: " << e.what()
                  << std::endl;
    }
    return b;
}

void print_heading(
    boost::gregorian::date date,
    int span,
    int indent
    )
{
    std::string mostr;

    // indent
    std::cout << std::setw(indent) << ' ';

    // print months in this span
    for (int i=0; i < span; ++i) {
        auto d = date + boost::gregorian::days(i);
        std::string tmpmo = std::string(d.month().as_short_string());
        if (mostr != tmpmo) {
            mostr = tmpmo;
            std::cout << mostr;
        }
        else {
            std::cout << "   ";
        }
    }
    std::cout << std::endl;

    // indent
    std::cout << std::setw(indent) << ' ';

    // print days in this span
    for (int i=0; i < span; ++i) {
        auto d = date + boost::gregorian::days(i);
        std::cout << " " << std::setw(2) << d.day();
    }
    std::cout << std::endl;

    // indent
    std::cout << std::setw(10) << ' ';

    // print separator
    for (int i=0; i < span; ++i) {
        std::cout << "---";
    }
    std::cout << std::endl;
}
