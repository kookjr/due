The packages in this directory are external projects. They are included by git submodule or copied. Rights belong to the original owner.

json/
    nlohmann/json is a header only json library.
    Site: https://github.com/nlohmann/json
    Version: ee4028b8
