[![pipeline status](https://gitlab.com/kookjr/due/badges/master/pipeline.svg)](https://gitlab.com/kookjr/due/commits/master)


# Due

Due is a simple command line bill reminder. In a console window it allows you to show what bills are coming due in the next few weeks.

## Dependencies

This application has only a few dependencies
  * Modern C++ compiler featuring C++11 feature set
  * [Boost](http://www.boost.org/) datetime and program options
  * cmake

If using `apt` the minimum libraries are:
  * libboost-program-options-dev
  * libboost-date-time-dev

## License

GNU GENERAL PUBLIC LICENSE, Version 3. See COPYING file in source distribution.

# Applicaiton

Due takes an `input_file` that describes bills and which day they are due. Look at `sample_input.txt` for an example. If one is not specified on the command line `$HOME/.bills` is used.

```
Usage: ./due [options]
Options:
  --help                help message
  --days arg            days from today for due bills
  --input arg           input bill file
  --paid arg            paid bill file

Defaults:
    days  = 22 or based terminal width if available
    input = $HOME/.bills
    paid  = $HOME/.paid
```

## Input Files

### Bills File

The input file uses is [JSON](http://json.org/). Here is a three bill example.

```javascript
   {"bills":[
     {"name":"Morgage",  "days-prior":5,  "bill-type":"a", "dom":13},
     {"name":"Spotify",  "days-prior":10, "bill-type":"y", "doy":[6,30]},
     {"name":"DonateFSF","days-prior":2,  "bill-type":"a", "dow":"Wed"}
   ]}
```

Each bill has the folling fields:
  * `name` - The bill name
  * `days-prior` - Beginning of marks before bill date
  * `bill-type` - Character to indicate bill span in output
  * The last field specifies the occurrence of the bill. It's value varies based on type
    * `dom` - Day of Month, day number e.g. 3
    * `dow` - Day of Week, weekday name, i.e. Sun, Mon, Tue, ...
    * `day` - Day of Year, month and day number e.g. 6,30 for June 30th

#### Rules

Rules for input file format:
  * The Bill Type can be any lower case letter. Days leading up to the bill due date will use this letter. The actual bill date will use the same letter, capitalized.
  * No comments are allowed.
  * Bill names are at most 10 characters.

### Paid File

The input file uses is [JSON](http://json.org/). Here is an example.

```javascript
   {"bills":[
     {"name":"Morgage",  "paid":"2019-06-10"},
     {"name":"DonateFSF","paid":"2019-05-28"}
   ]}
```

#### Rules
+ The `paid` field is in the format CCYY-MM-DD.
+ The `name` must match the bills file.
+ No comments are allowed.
+ Bill names are at most 10 characters.

# Sample Output

The following output comes from a bills file with the information from the table above. Note how the Insurance bill consistes of asterisks, indicating it's paid. The same is true for the first week of DonateFSF.

![Sample Output](docassets/sample_bills.png)

# Installation

Currently this application can be built on a Linux system, and installed with `make install`

## Building from Source

### Linux

Make sure boost datetime library and program options header are installed.

Clone this repoistory and build it.

```
  $ git clone https://gitlab.com/kookjr/due
  $ cd due
  $ mkdir build && cd build && cmake .. && make
  $ sudo make install
```


### Termux, Android terminal emulator and Linux environment

[Termux](https://termux.com/) is a great phone environment that can compile run command line utilities like `due`. Use the following commands to build and install.

```
  $ pkg install git clang cmake boost-dev/stable
  $ git clone https://gitlab.com/kookjr/due
  $ cd due
  $ mkdir b
  $ cd b
  $ cmake ..
  $ make
  $ cd ..
  $ cp b/due $HOME/../usr/bin
  $ # for a sample bills file
  $ cp sample_bills.txt $HOME/.bills
  $ # to test
  $ due
```
