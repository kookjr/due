//    Copyright (C) 2017 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Due; a bill reminder program.
//
//    Due is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Due is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Due.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <boost/program_options.hpp>

#include "clopts.hpp"

namespace po = boost::program_options;

std::tuple<bool,int> parse_command_line(
    int argc, char* argv[],
    std::string& bill_file,
    std::string& paid_file,
    int& day_range)
{
    try {
        po::options_description desc("Options");
        desc.add_options()
            ("help", "help message")
            ("days", po::value<int>(), "days from today for due bills")
            ("input", po::value<std::string>(), "input bill file")
            ("paid", po::value<std::string>(), "paid bill file")
        ;
        po::variables_map vm;
        const po::positional_options_description p; // no positional options
        po::store(
            po::command_line_parser(argc, argv).
            options(desc).
            positional(p).
            run(), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << "Usage: " << argv[0] << " [options]" << std::endl
                      << desc << std::endl;
            std::cout << "Defaults:" << std::endl
                      << "    days  = 22" << std::endl
                      << "    input = $HOME/.bills" << std::endl
                      << "    paid  = $HOME/.paid" << std::endl;

            return std::make_tuple(true, EXIT_SUCCESS);
        }

        if (vm.count("days"))
            day_range = vm["days"].as<int>();
        if (vm.count("input"))
            bill_file = vm["input"].as<std::string>();
        if (vm.count("paid"))
            paid_file = vm["paid"].as<std::string>();
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        return std::make_tuple(true, EXIT_FAILURE);
    }
    catch(...) {
        std::cerr << "Exception of unknown type!" << std::endl;
        return std::make_tuple(true, EXIT_FAILURE);
    }

    return std::make_tuple(false, -1);
}
